/*
 * Main.cpp
 *
 *  Created on: Fall 2019
 */

#include <stdio.h>
#include <math.h>
#include <pthread.h>
#include <CImg.h>
#include <string.h>
#include <unistd.h>

using namespace cimg_library;
double total;

void *process_chunk(void* v); // Function to execute the algorithm for a chunk of the image

// Data type for image components
// FIXME: Change this type according to your group assignment
typedef float data_t;

// We had to declare these variables here to be able to use them in the process_chunk function

data_t *pRsrc, *pGsrc, *pBsrc; // Pointers to the R, G and B components
data_t *pRdest, *pGdest, *pBdest;
data_t *pDstImage; // Pointer to the new image pixels
data_t pLsrc;	//variable Li, para convertir a blanco & negro
data_t pLdest;	//variable Li', para invertir la imagen.

const char *SOURCE_IMG = "bailarina.bmp";
const char *DESTINATION_IMG = "bailarina-blanco-negro.bmp";
char inicio[] = "./diffImages ";

//Imagen que se quiere comparar,normalemente es la misma que la de salida
char DESTINATION_IMG_COMPARE[] = "bailarina-blanco-negro.bmp ";
//Imagen con la que se quiere comparar.
char DESTINATION_IMG_COMPARE_WITH[] = "bailarina-blanco-negro-single.bmp";

const int REPETITIONS = 80;

const int NUMBER_THREADS = 8;

double chunkSize; // Size of the chunk of image each thread has to process

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER; // Locking mecanism to ensure thread-safety

int main() {
	//for (uint i = 0; i < 20; i++){
		// Open file and object initialization
	CImg<data_t> srcImage;
	try
	{
		srcImage.assign(SOURCE_IMG);
	}catch(CImgIOException &exception){
		perror("The source image file does not exist\n");
		exit(-2);
	}

	uint width, height; // Width and height of the image
	uint nComp; // Number of image components


	/***************************************************
	 * TODO: Variables initialization.
	 *   - Prepare variables for the algorithm
	 *   - This is not included in the benchmark time
	 */

	// Variables for image processing
	struct timespec tStart, tEnd;
    double dElapsedTimeS;
	

	srcImage.display(); // Displays the source image
	width  = srcImage.width(); // Getting information from the source image
	height = srcImage.height();
	nComp  = srcImage.spectrum(); // source image number of components
				// Common values for spectrum (number of image components):
				//  B&W images = 1
				//	Normal color images = 3 (RGB)
				//  Special color images = 4 (RGB and alpha/transparency channel)


	// Variables for multithreading tasks:
	int threads_array[NUMBER_THREADS];
	chunkSize = (width * height)/NUMBER_THREADS;
    pthread_t thread_id[NUMBER_THREADS];


	// Allocate memory space for destination image components
	pDstImage = (data_t *) malloc (width * height * nComp * sizeof(data_t));
	if (pDstImage == NULL) {
		perror("Allocating destination image");
		exit(-2);
	}

	// Pointers to the componet arrays of the source image
	pRsrc = srcImage.data(); // pRcomp points to the R component array
	pGsrc = pRsrc + height * width; // pGcomp points to the G component array
	pBsrc = pGsrc + height * width; // pBcomp points to B component array

	// Pointers to the RGB arrays of the destination image
	pRdest = pDstImage;
	pGdest = pRdest + height * width;
	pBdest = pGdest + height * width;


	/***********************************************
	 * TODO: Algorithm start.
	 *   - Measure initial time
	 */
	if(clock_gettime(CLOCK_REALTIME, &tStart) == -1)
	{
		perror("ERROR: clock_gettime\n");
		exit(-1);
	}

	for (int j = 0; j < REPETITIONS; j++)
	{

		for (int i = 0; i < NUMBER_THREADS; i++)
		{
		  	threads_array[i]=i;
   
		  	int return_value = pthread_create(&thread_id[i], NULL, &process_chunk, &threads_array[i]);

			if (return_value)
			{	
				printf("ERROR: pthread_create error code: %d.\n", return_value);
				exit(EXIT_FAILURE);
			}
		}

		for (int i = 0; i < NUMBER_THREADS; i++)
		{
			pthread_join(thread_id[i], NULL);
			//pthread_exit(NULL);
    	}

	}
	nComp=1; //La imagen es blanco y negro.

	/***********************************************
	 * TODO: End of the algorithm.
	 *   - Measure the end time
	 *   - Calculate the elapsed time
	 */
	if(clock_gettime(CLOCK_REALTIME, &tEnd) == -1)
	{
		perror("ERROR: clock_gettime\n");
		exit(-1);
	}

	dElapsedTimeS = (tEnd.tv_sec - tStart.tv_sec);
    dElapsedTimeS += (tEnd.tv_nsec - tStart.tv_nsec) / 1e+9;
	total += dElapsedTimeS;
	printf("Elapsed time    : %f s.\n", dElapsedTimeS);
		
	// Create a new image object with the calculated pixels
	// In case of normal color images use nComp=3,
	// In case of B/W images use nComp=1.
	CImg<data_t> dstImage;
	try
	{
		dstImage.assign(pDstImage, width, height, 1, nComp);
		// Store destination image in disk
		dstImage.save(DESTINATION_IMG); 
	}catch(CImgIOException &exception){
		perror("Unable to create the destination image; MISSING FOLDER\n");
		exit(-2);
	}


	// Display destination image
	dstImage.display();
	
	// Free memory
	free(pDstImage);

	printf("Elapsed time    : %f s.\n", total);

	char *start = strcat(inicio,DESTINATION_IMG_COMPARE);
	char *call = strcat(start,DESTINATION_IMG_COMPARE_WITH);
	system(call);

	return 0;
	
}

void *process_chunk(void* var)
{
	pLsrc = 0;
	pLdest = 0;

	uint tid = *((int*)var); //Thread id (t-id)

	uint beginning_point = tid * chunkSize; // Pixel of the image where the loop has to begin
	uint ending_point = ((tid+1) * chunkSize); // Pixel of the image where the loop has to end

	pthread_mutex_lock(&mutex); // Uses a pthread_mutex_t type to lock a part of the code

	for (tid = beginning_point; tid < ending_point; tid++)
	{

		//Convertir a blanco & negro.
		pLsrc = *(pRsrc + tid) * 0.3 + *(pGsrc + tid) * 0.59 + *(pBsrc + tid) * 0.11;

		//Invertir.
		pLdest = 255 - pLsrc;	

		//Asignar los nuevos valores a los pixeles.
		*(pRdest + tid) = pLdest;

	}

	pthread_mutex_unlock(&mutex); // Uses a pthread_mutex_t type to unlock that part of the code
	return NULL;
}
